import exit from "../assets/iconos/salir.svg"


function Boton_Salir() {
  return (
    <a className="shadow_t btn btn-light p-1 me-3 text-decoration-none text-black cursor" height="50px" style={{height:"75px", width:"80px", borderRadius:"10px", }} href="/Login"> 
        <img src={exit} alt="salir"  width="70px"/>
    </a>
  )
}

export default Boton_Salir