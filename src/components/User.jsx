import React from "react";
import usuario from "../assets/iconos/user.svg";

function User({ grande }) {
  {if (grande === true) {
      return (
        <div className="d-flex align-items-center  justify-content-center p-2 mt-3 me-3 bg-light bg-gradient rounded border border-info  border-opacity-10 border-2">
          <div className="d-flex flex-column me-3 fs-5 text-center fw-bold">
            <p className="m-0">Juan Ramirez Gregorio Feliz</p>
            <p className="m-0">Bioanalista</p>
          </div>
          <div
            className="rounded-circle  d-flex justify-content-center align-items-center"
            style={{ height: "60px", width: "60px" }}>
            <img className="m-0 p-0" src={usuario} alt="usuario" width="60px" />
          </div>
        </div>
      )
    } else {
      return (
        <div className="d-flex align-items-center justify-content-center px-3 py-1 mt-1 me-3 bg-light bg-gradient rounded border border-info  border-opacity-10 border-2">
          <div className="d-flex align-items-center me-3 fs-5 text-center fw-bold">
            <p className="m-0">Juan Ramirez Gregorio Feliz</p>
            <p className="mx-3 my-0">|</p>
            <p className="m-0">Bioanalista</p>
          </div>
          <div
            className="rounded-circle  d-flex justify-content-center align-items-center"
            style={{ height: "45px", width: "60px" }}>
            <img className="m-0 p-0" src={usuario} alt="usuario" width="60px" />
          </div>
        </div>
      )
    }
  }
}

export default User;
