import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import guardar from "../assets/iconos/registro.png"
import x from "../assets/iconos/x.svg";
import close from "../assets/iconos/close.png"



function Examen() {
    return (
        <main className='fondo_20 min-vh-100 d-flex flex-column align-items-center'>
            <section className="d-flex justify-content-between w-100">
                <Menu />
                <User grande={false} />
            </section>
            <section className=" py-3 px-3 w-95 rounded mt-1 bg-white">
                <div className='d-flex w-100 justify-content-around w-100' >
                    <div className="w-50">
                        <p className='m-0 letra_2'><b className='me-1'>Nombre:</b> Pedro Egnis Medina Camacho</p>
                        <p className='m-0 letra_2'><b className='me-1'>C.I:</b>  30565353</p>
                        <p className='m-0 letra_2'><b className='me-1'>Correo Electronico:</b>  medinapedrito2@gmail.com</p>
                        <p className='m-0 letra_2'><b className='me-1'>Edad:</b>  19</p>
                        <p className='m-0 letra_2'><b className='me-1'>Fecha de Nacimiento:</b>  19/05/2000</p>
                        <p className='m-0 letra_2'><b className='me-1'>Sexo:</b>  Masculino</p>
                        <p className='m-0 letra_2'><b className='me-1'>Telefono:</b>  0414-1250451</p>
                        <p className='m-0 letra_2'><b className='me-1'>Dirección:</b>  3er transversal de Rupertolugo, Catica, Caracas, Venezuela</p>
                    </div>
                    <div >
                        <p className='m-0 letra_2'><b className='me-1'>Orden:</b>  6041-051</p>
                        <p className='m-0 letra_2'><b className='me-1'>Paciente:</b>  0015</p>
                        <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/04/2023</p>
                        <p className='m-0 letra_2'><b className='me-1'>Encargado:</b>  Luis Miguel Lopez Labrador </p>
                        <p className='m-0 letra_2'><b className='me-1'>Fecha de Finalización:</b> - </p>
                        <p className='m-0 letra_2'><b className='me-1'>Examen:</b>  Examen de orina</p>
                        <p className='m-0 letra_2'><b className='me-1'>Estado:</b> <img className="cursor" width="50px" height="30px" src={x} alt="eliminar"/> Incompleto</p>
                        <p className='m-0 letra_2'><b className='me-1'>Medico:</b> - </p>
                    </div>
                </div>
                <div className='hr'></div>
                <h3 className='text-center'>Muestras Asignadas</h3>
                <div className='d-flex w-100 px-5'>
                    <div>
                        <p className='m-0 letra_2'><b className='me-1'>Tipo de Muestra:</b>  Muestra de Orina</p>
                        <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/05/2023</p>
                        <p className='m-0 letra_2'><b className='me-1'>Fecha de Extracción:</b>  19/05/2023</p>
                        <p className='m-0 letra_2'><b className='me-1'>Número:</b>  1 </p>
                        <p className='m-0 letra_2'><b className='me-1'>Lote:</b> - </p>
                        <p className='m-0 letra_2'><b className='me-1'>Interna:</b> Positivo</p>
                    </div>
                </div>
                <h3 className='text-center'>Resultados</h3>
                <div className='py-3'>
                    <h4 className='text-center'>- - - - No existen - - - -</h4>
                </div>
                <div className='d-flex'>
                    <button className="mx-auto py-1 px-4 fw-bold bEliminar">
                        <img
                            className="me-2"
                            width="40px"
                            height="40px"
                            src={close}
                            alt="guardar"
                        />
                        Eliminar Examen
                    </button>
                    
                </div>
            </section>
            <section className="align-self-start mt-auto justify-self-end">
                <Reloj grande={false} />
            </section>

        </main>
    )
}

export default Examen