import contraseña_2 from '../assets/iconos/contraseña_2.svg'
import PerfilLogo from '../assets/iconos/PerfilLogo.svg'



function Login() {
  return (
    <main className="w-100 vh-100 d-flex justify-content-center align-items-center fondo">
    <form className=" d-flex justify-content-between align-items-center flex-column bg-white border border-info border-1 rounded form_form"  id="Formulario" action="/login/" method="POST">  
        <h2 className="text-uppercase fs-2 fw-bold m-0">Iniciar Sesión</h2>
        {/* Campo de Usuario */}
        <div className="d-flex w-100" id="Input_Usuario">      
            <label className="me-1 cursor" for="Usuario"><img width="40" src={PerfilLogo} alt="perfil"/></label>
            <input className="form-control border border-2 border-dark-subtle" type="text" name="Usuario" id="Usuario" placeholder="Usuario" required/>
        </div>
        {/* Campo de la Contraseña */}
        <div className="d-flex w-100 " id="Input_Contraseña1">         
            <label  className="me-1 cursor" for="Contraseña1"><img width="40" src={contraseña_2} alt="contraseña"/></label>
            <input  className="form-control border border-2 border-dark-subtle" type="password" name="Contraseña1" id="Contraseña1" placeholder="Contraseña" required/>
        </div>
        {/* Boton Para Enviar Datos*/}
        <button  type="button" onClick={()=>location.href ='/'}  className="w-100 fw-bold btn btn-primary" id="Boton">Enviar</button> 
    </form>
  </main>
  )
}

export default Login