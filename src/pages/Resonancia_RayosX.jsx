import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import guardar from "../assets/iconos/registro.png"
import mas from "../assets/iconos/plus.png"

export default function Resonancia_RayosX() {
  return (
    <main className='fondo_20 min-vh-100 d-flex flex-column align-items-center'>
      <section className="d-flex justify-content-between w-100">
        <Menu />
        <User grande={false} />
      </section>
      <section className=" py-3 px-3 w-95 rounded mt-1 bg-white vh-85">
        <div className='d-flex w-100 justify-content-around'>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Nombre:</b> Pedro Egnis Medina Camacho</p>
            <p className='m-0 letra_2'><b className='me-1'>C.I:</b>  30565353</p>
            <p className='m-0 letra_2'><b className='me-1'>Edad:</b>  19</p>
            <p className='m-0 letra_2'><b className='me-1'>Sexo:</b>  Masculino</p>
          </div>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Orden:</b>  6041-051</p>
            <p className='m-0 letra_2'><b className='me-1'>Paciente:</b>  0015</p>
            <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/04/2023</p>
            <p className='m-0 letra_2'><b className='me-1'>Medico:</b>  Juan Ramirez Gregorio Feliz |  Bioanalista</p>
          </div>
        </div>
        <div className='hr'></div>
        <div className='d-flex flex-column justify-content-between vh-60 position-relative'>
          <h2 className='text-center m-0 mb-2'>Radiografía de Cráneo</h2>
          <div className='d-flex  justify-content-evenly align-items-center p-2 my-2 '>
            <label htmlFor="" className='fw-bold fs-5'>Nombre</label>
            <input type="text" className='form-control border border-secondary' style={{ width: "300px", height: "30px" }} />
            <input type="file" className='form-control border border-secondary' id="" style={{ width: "400px" }} />
          </div>
          <div className='d-flex align-items-center my-2 w-100'>
            <label className='fw-bold fs-4 me-3' htmlFor="">Observaciones</label>
            <textarea className='form-control border border-secondary' name="" rows="5"></textarea>
          </div>
          <button className="mx-auto py-1 px-4 fw-bold bGuardar">
            <img
              className="me-2"
              width="40px"
              height="40px"
              src={guardar}
              alt="guardar"
            />
            Guardar
          </button>

          <button className="mx-auto py-1 px-4 fw-bold bAgregar position-absolute top-0 end-0">
            <img
              className="me-2"
              width="40px"
              height="40px"
              src={mas}
              alt="guardar"
            />
            Agrar Otro archivo
          </button>
        </div>

      </section>
      <section className="align-self-start mt-auto justify-self-end">
        <Reloj grande={false} />
      </section>

    </main>
  )
}
