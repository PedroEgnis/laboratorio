import Reloj from "../components/Reloj";
import lupa from "../assets/iconos/loupe.png";
import x from "../assets/iconos/x.svg";
import alerta from "../assets/iconos/alerta.svg";
import guardar from "../assets/iconos/registro.png";
import Menu from '../components/Menu'
import User from '../components/User'

function Registro_Examenes() {
  return (
    <main className="fondo_20 min-vh-100 d-flex flex-column align-items-center">
      <section className="d-flex justify-content-between w-100">
        <Menu />
        <User grande={false} />
      </section>
      <form className="w-100 d-flex flex-column align-items-center justify-content-center">
      <h1 className="fw-bold">Registros de Examenes Clinicos</h1>
      <section className="d-flex flex-column align-items-center bg-white  p-3 w-95">
        <h4 className="mx-auto fs-2 text-dark fw-semibold mb-1">Paciente</h4>

        <div className="row mb-2 w-100">
          <div className="col-2  d-flex  justify-content-center align-items-center">
            <label className="fs-5 fw-bold me-2" htmlFor="c.i ">
              C.I
            </label>
            <input
              id="c.i"
              className="fs-5 w-75 px-2 text-center"
              type="number"
              value="30565353"
              aria-label="Disabled input example"
              disabled

            />
          </div>

          <div className="col-5 d-flex justify-content-center align-items-center">
            <label className="fs-5 fw-bold me-2" htmlFor="nombre">
              Nombre
            </label>
            <input
              id="nombre"
              className="fs-5 w-75 px-2 text-center"
              type="text"
              value="Pedro Egnis Medina Camaho"
              aria-label="Disabled input example"
              disabled
            />
          </div>

          <div className="col-2  d-flex justify-content-center align-items-center">
            <label className="fs-5 fw-bold me-2" htmlFor="edad">
              Edad
            </label>
            <input
              id="edad"
              className="fs-5 w-50 px-2 text-center"
              type="number"
              value="19"
              aria-label="Disabled input example"
              disabled
            />
          </div>

          <div className="col-2 ms-5 ">
            <button type="button" className=" fw-bold p-1 d-flex align-items-center justify-content-center bbuscar">
              {" "}
              <img width="50px" height="50px" src={lupa} alt="Buscar" />
              Buscar Paciente
            </button>
          </div>
        </div>

        <div className="row  w-100 ">
          <div className="col-3  d-flex  justify-content-center align-items-center">
            <label className="fs-5 fw-bold me-2" htmlFor="telefono">
              Telefono
            </label>
            <input
              id="c.i"
              className="fs-5 w-60 px-2 text-center"
              type="number"
              value="04241250451"
              aria-label="Disabled input example"
              disabled />
          </div>

          <div className="col-3 d-flex justify-content-center">
            <label className="fs-5 fw-bold me-3" htmlFor="sexo">
              Sexo
            </label>
            <input
              id="sexo"
              className="fs-5 w-40 px-2 text-center"
              type="text"
              value="Masculino"
              aria-label="Disabled input example"
              disabled
            />
          </div>

          <div className="col-6 justify-content-center">
            <label className="fs-5 fw-bold me-3" htmlFor="correo">
              Correo
            </label>
            <input
              type="email"
              id="correo"
              className="fs-5 w-80 px-2 text-center"
              value="medinapedrito2@gmail.com"
              aria-label="Disabled input example"
              disabled
            />
          </div>
        </div>
      </section>

      <section className="bg-white mt-2 p-2 w-95">
        <h4 className="mx-auto fs-2 text-dark fw-semibold mb-1 text-center">
          Examenes
        </h4>
        <div className=" my-1 w-100 d-flex justify-content-center ">
          <button type="button" className="mx-5 py-1 px-4 fw-bold bAgregar">
            {" "}
            <img
              className="me-2"
              width="50px"
              height="50px"
              src={lupa}
              alt="agregar"
            />{" "}
            Buscar Examen
          </button>
          <button type="button" className="mx-5 py-1 px-4 fw-bold bGuardar">
            <img
              className="me-2"
              width="50px"
              height="50px"
              src={guardar}
              alt="guardar"
            />
            Registrar
          </button>
        </div>
        <div className="tabla">
          <table className=" w-100 mt-2 ">
            <thead className=" text-center">
              <tr>
                <th className="border fs-5 border-dark th-1">Codigo</th>
                <th className="border fs-5 border-dark th-2">Examen</th>
                <th className="border fs-5 border-dark th-3">
                  Fecha de Registro
                </th>
                <th className="border fs-5 border-dark th-4">Estado</th>
                <th className="th-5"></th>
              </tr>
            </thead>
            <tbody className="text-center">
              <tr>
                <td className="border fs-5 border-dark th-1"> 165-15</td>
                <td className="border fs-5 border-dark th-2"> Examen de Orina</td>
                <td className="border fs-5 border-dark th-3"> 17/03/2003</td>
                <td className="border fs-5 border-dark th-4"> Incompleto</td>
                <td className="th-5 d-flex align-items-center justify-content-around">
                  <img
                    className="cursor accion"
                    width="40px"
                    height="30px"
                    src={x}
                    alt="eliminar"
                  />
                  <img
                    className="cursor"
                    width="40px"
                    height="30px"
                    src={alerta}
                    alt="alerta"
                  />
                </td>
              </tr>


            </tbody>
            <tbody className="text-center">
              <tr>
                <td className="border fs-5 border-dark th-1"></td>
                <td className="border fs-5 border-dark th-2"></td>
                <td className="border fs-5 border-dark th-3"></td>
                <td className="border fs-5 border-dark th-4"></td>
                <td className="th-5 d-flex align-items-center justify-content-around"></td>
              </tr>
            </tbody>
            <tbody className="text-center">
              <tr>
                <td className="border fs-5 border-dark th-1"></td>
                <td className="border fs-5 border-dark th-2"></td>
                <td className="border fs-5 border-dark th-3"></td>
                <td className="border fs-5 border-dark th-4"></td>
                <td className="th-5 d-flex align-items-center justify-content-around"></td>
              </tr>
            </tbody>
            <tbody className="text-center">
              <tr>
                <td className="border fs-5 border-dark th-1"></td>
                <td className="border fs-5 border-dark th-2"></td>
                <td className="border fs-5 border-dark th-3"></td>
                <td className="border fs-5 border-dark th-4"></td>
                <td className="th-5 d-flex align-items-center justify-content-around"></td>
              </tr>
            </tbody>
            <tbody className="text-center">
              <tr>
                <td className="border fs-5 border-dark th-1"></td>
                <td className="border fs-5 border-dark th-2"></td>
                <td className="border fs-5 border-dark th-3"></td>
                <td className="border fs-5 border-dark th-4"></td>
                <td className="th-5 d-flex align-items-center justify-content-around"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>
      </form>
      
      <section className="align-self-start mt-auto justify-self-end">
        <Reloj grande={false} />
      </section>
    </main>
  );
}

export default Registro_Examenes;
