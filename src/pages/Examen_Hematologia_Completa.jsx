import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import guardar from "../assets/iconos/registro.png"

function Examen_Hematologia_Completa() {
  return (
    <main className='fondo_20 min-vh-100 d-flex flex-column align-items-center'>
      <section className="d-flex justify-content-between w-100">
        <Menu />
        <User grande={false} />
      </section>
      <section className=" py-3 px-3 w-95 rounded mt-1 bg-white">
        <div className='d-flex w-100 justify-content-around'>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Nombre:</b> Pedro Egnis Medina Camacho</p>
            <p className='m-0 letra_2'><b className='me-1'>C.I:</b>  30565353</p>
            <p className='m-0 letra_2'><b className='me-1'>Edad:</b>  19</p>
            <p className='m-0 letra_2'><b className='me-1'>Sexo:</b>  Masculino</p>
          </div>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Orden:</b>  6041-051</p>
            <p className='m-0 letra_2'><b className='me-1'>Paciente:</b>  0015</p>
            <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/04/2023</p>
            <p className='m-0 letra_2'><b className='me-1'>Medico:</b>  Juan Ramirez Gregorio Feliz |  Bioanalista</p>
          </div>
        </div>
        <div className='hr'></div>
        <div>
          <h2 className='text-center m-0 mb-2'>Hematología Completa</h2>
          <div className='d-flex bg-secondary-subtle border border-dark justify-content-between px-2'>
            <p className='m-0 letra' style={{ width: "190px" }}>Analisis</p>
            <p className='my-0 letra ' style={{ width: "150px", marginLeft: "50px" }}>Resultado</p>
            <p className='m-0 letra' style={{ width: "50px" }}>Unidades</p>
            <p className='m-0 letra' style={{ width: "180px" }}>Valor Referencial</p>
          </div>
          <div>
            <div className='d-flex  justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>HEMATIES</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px" }} />
              <p className='m-0 letra' style={{ width: "50px" }}>mm3</p>
              <p className='m-0 letra' style={{ width: "180px" }}>3.80-5.50 X 10.6</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>HEMOGLOBINA</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>g/dl</p>
              <p className='m-0 letra' style={{ width: "180px" }}>11.0-16.5</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>HEMATOCRITO</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>%</p>
              <p className='m-0 letra' style={{ width: "180px" }}>35-51</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>VCM</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>fl </p>
              <p className='m-0 letra' style={{ width: "180px" }}>80-100</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>HCM</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>pg</p>
              <p className='m-0 letra' style={{ width: "180px" }}>27 - 34</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>CHCM</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>g/dl</p>
              <p className='m-0 letra' style={{ width: "180px" }}>32-36</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>CONTAJE DE BLANCOS </label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>mm3</p>
              <p className='m-0 letra' style={{ width: "180px" }}>3.50-10.0 x 1000</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>SEGMENTADOS</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>%</p>
              <p className='m-0 letra' style={{ width: "180px" }}>50-70</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>LINFOCITOS</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>%</p>
              <p className='m-0 letra' style={{ width: "180px" }}>20-40</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>CELULAS MEDIA</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>%</p>
              <p className='m-0 letra' style={{ width: "180px" }}>3-14</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>LEUCOCITOS</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>cel/mm3 </p>
              <p className='m-0 letra' style={{ width: "180px" }}>4.500 - 10.500</p>
            </div>
            <div className='d-flex justify-content-between px-2 my-2'>
              <label htmlFor="" className='fw-bold' style={{ width: "190px" }}>CONTAJE DE PLAQUETAS</label>
              <input type="text" className='form-control border border-secondary' style={{ width: "200px", height: "30px", }} />
              <p className='m-0 letra' style={{ width: "50px" }}>cel/mm3 </p>
              <p className='m-0 letra' style={{ width: "180px" }}>150-450 x 1000</p>
            </div>
            <div className='d-flex align-items-center my-2 w-100'>
              <label className='fw-bold fs-4 me-3' htmlFor="">Observaciones</label>
              <textarea className='form-control border border-secondary' name="" rows="5"></textarea>
            </div>
          </div>
        </div>
        <button className="mx-auto py-1 px-4 fw-bold bGuardar">
          <img
            className="me-2"
            width="40px"
            height="40px"
            src={guardar}
            alt="guardar"
          />
          Guardar
        </button>
      </section>
      <section className="align-self-start mt-auto justify-self-end">
        <Reloj grande={false} />
      </section>

    </main>
  )
}

export default Examen_Hematologia_Completa