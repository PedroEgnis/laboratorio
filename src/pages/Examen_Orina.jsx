import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import guardar from "../assets/iconos/registro.png"

function Examen_Orina() {
  return (
    <main className='fondo_20 min-vh-100 d-flex flex-column align-items-center'>
      <section className="d-flex justify-content-between w-100">
        <Menu />
        <User grande={false} />
      </section>
      <section className=" py-3 px-3 w-95 rounded mt-1 bg-white">
        <div className='d-flex w-100 justify-content-around'>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Nombre:</b> Pedro Egnis Medina Camacho</p>
            <p className='m-0 letra_2'><b className='me-1'>C.I:</b>  30565353</p>
            <p className='m-0 letra_2'><b className='me-1'>Edad:</b>  19</p>
            <p className='m-0 letra_2'><b className='me-1'>Sexo:</b>  Masculino</p>
          </div>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Orden:</b>  6041-051</p>
            <p className='m-0 letra_2'><b className='me-1'>Paciente:</b>  0015</p>
            <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/04/2023</p>
            <p className='m-0 letra_2'><b className='me-1'>Medico:</b>  Juan Ramirez Gregorio Feliz |  Bioanalista</p>
          </div>
        </div>
        <div className='hr'></div>
        <div>
            <h2 className='text-center m-0 mb-2'>Uroanalisis</h2>
            <div className='d-flex bg-secondary-subtle border border-dark justify-content-between px-2'>
                <p className='m-0 letra' style={{width:"120px"}}>Analisis</p>
                <p className='my-0 letra ' style={{width:"150px", marginLeft:"50px"}}>Resultado</p>
                <p className='m-0 letra' style={{width:"50px"}}>Unidades</p>
                <p className='m-0 letra' style={{width:"180px"}}>Valor Referencial</p>
            </div>
            <div>
                <h5 className='text-center'>Macroscopio</h5>
                <div className='d-flex  justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>COLOR</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>ASPECTO</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
            </div>
            <div>

                <h5 className='text-center'>Quimico</h5>

                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>DENSIDAD</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}>gr/ml</p>
                    <p className='m-0 letra' style={{width:"180px"}}>1003 - 1030</p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>PROTEINAS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>PH</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}>5 - 7</p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>SANGRE</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>GLUCOSA</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>NITRATOS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
            </div>
            <div>
                <h5 className='text-center'>Microscópico</h5>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>BACTERIAS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>LEUCOCITOS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}>Obser./Campo</p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>ERITROSITOS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px", }}/>
                    <p className='m-0 letra' style={{width:"50px"}}>Obser./Campo</p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>HEMATIES</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}>Obser./Campo</p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>MUCINA</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}></p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex justify-content-between px-2 my-2'>
                    <label htmlFor="" className='fw-bold' style={{width:"120px"}}>EPIT. PLANAS</label>
                    <input type="text" className='form-control border border-secondary' style={{width:"200px", height:"30px"}}/>
                    <p className='m-0 letra' style={{width:"50px"}}>Obser./Campo</p>
                    <p className='m-0 letra' style={{width:"180px"}}></p>
                </div>
                <div className='d-flex align-items-center my-2 w-100'>
            <label className='fw-bold fs-4 me-3' htmlFor="">Observaciones</label>
            <textarea className='form-control border border-secondary' name=""   rows="5"></textarea>
          </div>
            </div>
        </div>
        <button className="mx-auto py-1 px-4 fw-bold bGuardar">
            <img
              className="me-2"
              width="40px"
              height="40px"
              src={guardar}
              alt="guardar"
            />
            Guardar
          </button>
      </section>
      <section className="align-self-start mt-auto justify-self-end">
        <Reloj grande={false} />
      </section>

    </main>
  )
}

export default Examen_Orina