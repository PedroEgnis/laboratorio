import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import lupa from "../assets/iconos/lupa.svg"
import flecha_izquierda from "../assets/iconos/flecha_left.svg"
import flecha_derecha from "../assets/iconos/flecha_right.svg"
import bien from "../assets/iconos/bien.svg"
import todas from "../assets/iconos/todas.svg"
import todas_active from "../assets/iconos/todas_active.svg"
import mal from "../assets/iconos/mal.svg"
import x from "../assets/iconos/x.svg";


function Captura_Resultados() {
    return (
        <main className="fondo_20 min-vh-100 d-flex flex-column align-items-center">
            <section className="d-flex justify-content-between w-100">
                <Menu />
                <User grande={false} />
            </section>
            <section className='w-95 d-flex flex-column  vh-85 bg-white p-3 rounded mt-2'>
                <div className="w-100 d-flex">
                    <nav className='d-flex my-0 mx-auto w-80 border border-secondary-subtle rounded-pill' style={{ height: "35px" }}>
                        <select name="Muestra" className=" px-2 border-0 rounded-pill bg-secondary-subtle fs-6 text-center" id="Muestra">
                            <option selected value="1">Cedula</option>
                            <option value="2">Nombre</option>
                        </select>
                        <input type="text" className='mx-3 border-0 w-80 buscador' />
                        <button className='border-0 rounded-pill bg-secondary-subtle' style={{ width: "80px" }}><img src={lupa} alt="buscar" /></button>
                    </nav>
                    <div className='d-flex fs-6 align-items-center px-3'>
                        <p className='m-0 mx-1 letra'>1-50</p>
                        <p className='m-0 mx-1 letra'>de</p>
                        <p className='m-0 mx-1 letra'>100</p>
                        <p className='m-0 mx-1 cursor'><img src={flecha_izquierda} alt="flecha" /></p>
                        <p className='m-0 mx-1 cursor'><img src={flecha_derecha} alt="flecha" /></p>
                    </div>
                </div>
                <div className='d-flex w-100 justify-content-between mt-2'>
                    <p className='cursor m-0 py-2 px-5 letra border-bottom border-primary border-4 text-primary' style={{ width: "230px" }} ><img className='me-2' width="25px" src={todas_active} />Todos</p>
                    <div className='d-flex px-3 align-items-center'>
                        <p className='m-0 mx-2 letra'>Desde</p>
                        <input className='form-control' type="date" style={{ height: "35px" }} />
                        <p className='m-0 mx-2 letra'>Hasta</p>
                        <input className='form-control' type="date" style={{ height: "35px" }} />
                    </div>
                </div>
                <div className='bg-body-secondary w-100 mt-3 overflow-auto d-flex flex-column' style={{ height: "420px" }}>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Heces">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Examen de Heces</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Orina">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Examen de orina</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Orina">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Examen de orina</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Hematologia_Completa">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Hematologia Completa</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Heces">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Examen de Heces</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Resonancia_RayosX">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Radiografía de Cráneo</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    <a className='w-100 border-top border-secondary-subtle px-3 d-flex justify-content-between align-items-center links flex-fill' style={{ minHeight: "40px" }} href="/Examen_Heces">
                        <img
                            className="cursor"
                            width="50px"
                            height="35px"
                            src={x}
                            alt="eliminar"
                        />
                        <p className='m-0 overflow-hidden' style={{ width: "400px", maxHeight: "30px", fontSize: "19px" }}>Pedro Egnis Medina Camacho </p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>30565353</p>
                        <p className='m-0 text-center overflow-hidden' style={{ width: "350px", maxHeight: "30px", fontSize: "19px" }}>Examen de Heces</p>
                        <p className='m-0 text-end overflow-hidden' style={{ width: "180px", maxHeight: "30px", fontSize: "19px" }}>20/01/2003</p>
                    </a>
                    
                </div>
            </section>
            <section className="align-self-start mt-auto justify-self-end">
                <Reloj grande={false} />
            </section>
        </main>
    )
}

export default Captura_Resultados