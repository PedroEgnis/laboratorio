import lupa from "../assets/iconos/loupe.png"
import guardar from "../assets/iconos/registro.png"
import Reloj from "../components/Reloj";
import Menu from '../components/Menu'
import User from '../components/User'
function Registro_Muestra() {
    return (
        <main className="fondo_2 min-vh-100 d-flex flex-column align-items-center">
            <section className="d-flex justify-content-between w-100">
                <Menu />
                <User grande={false} />
            </section>
            <h1>Registro de Muestras</h1>
            <form className='w-95 bg-white p-3 position-relative'>
                <h4 className='fs-3'>Paciente</h4>
                <div className='mb-3 d-flex' >
                    <div>
                        <label className="fs-5 fw-bold me-3" htmlFor="c.i ">C.I</label>
                        <input id="c.i" className="fs-5 text-center px-1" style={{ width: "150px" }} type="number" value="30565353" aria-label="Disabled input example" disabled readonly />
                    </div>

                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3 " htmlFor="nombre">Nombre</label>
                        <input id="nombre" className="fs-5 text-center px-1" type="text" style={{ width: "400px" }} value="Pedro Egnis Medina Camaho" aria-label="Disabled input example" disabled readonly />
                    </div>

                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3" htmlFor="edad">Edad</label>
                        <input id="edad" className="fs-5  text-center px-1" type="number"
                            style={{ width: "80px" }} value="19" aria-label="Disabled input example" disabled readonly />
                    </div>

                </div>
                <div className='d-flex'>
                    <div>
                        <label className="fs-5 fw-bold me-3" htmlFor="sexo">Sexo</label>
                        <input id="sexo" className="fs-5  text-center px-1" type="text"
                            style={{ width: "130px" }} value="Masculino" aria-label="Disabled input example" disabled readonly />
                    </div>


                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3" htmlFor="correo">Correo</label>
                        <input type="email" id="correo" className="fs-5  text-center px-1"
                            style={{ width: "400px" }} value="medinapedrito2@gmail.com" aria-label="Disabled input example" disabled readonly />
                    </div>

                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3" htmlFor="Telefono ">Telefono</label>
                        <input id="Telefono" className="fs-5 text-center px-1" style={{ width: "160px" }} type="number" value="04241250451" aria-label="Disabled input example" disabled readonly />
                    </div>
                </div>

                <button type="button" className=' b-registro-muestra fw-bold'><img src={lupa} alt="lupa" width="60px" /> Buscar</button>

                <div className="w-100 hr"></div>

                <h4 className='fs-3 '>Examen</h4>

                <div className=' d-flex' >
                    <div >
                        <label className="fs-5 fw-bold me-3" htmlFor="Codigo">Codigo</label>
                        <input id="Codigo" className="fs-5 text-center px-1" style={{ width: "100px" }} type="number" value="165561" aria-label="Disabled input example" disabled readonly />
                    </div>
                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3" htmlFor="Examen">Examen</label>
                        <input id="Examen" className="fs-5 text-center px-1" style={{ width: "250px" }} type="text" value="Hematologia" aria-label="Disabled input example" disabled readonly />
                    </div>
                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3 " htmlFor="fecha_registro">Fecha Registro  </label>
                        <input id="fecha_registro" className="fs-5 text-center px-1" type="date" style={{ width: "180px" }} value="2018-07-22" aria-label="Disabled input example" disabled readonly />
                    </div>
                </div>

                <div className="w-100 hr"></div>

                <h4 className='fs-3'>Muestra</h4>

                <div className='d-flex'>
                    <div className="d-flex ">
                        <label className="fs-5 fw-bold me-3" htmlFor="Muestra ">             Tipo de Muestra</label>
                        <select name="Muestra" className="form-select fs-5 text-center px-1" id="Muestra"
                            style={{ width: "200px" }}>
                            <option className="fs-5 text-center px-1" selected>Muestra</option>
                            <option className="fs-5 text-center px-1" value="1">Muestra de Orina</option>
                            <option className="fs-5 text-center px-1" value="2">Muestra de Heces</option>
                            <option className="fs-5 text-center px-1" value="3">Muestra de Sangre</option>
                        </select>
                    </div>
                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3 " htmlFor="fecha_registro">Fecha Registro  </label>
                        <input id="fecha_registro" className="fs-5 text-center px-1" type="date" style={{ width: "180px" }} value="2018-07-22" aria-label="Disabled input example" readonly />
                    </div>
                    <div className='ms-5'>
                        <label className="fs-5 fw-bold me-3 " htmlFor="fecha_extraccion">Fecha Extracción  </label>
                        <input id="fecha_extraccion" className="fs-5 text-center px-1" type="date" style={{ width: "180px" }} value="2018-07-22" aria-label="Disabled input example" readonly />
                    </div>
                </div>

                <div className='d-flex mt-2'>
                    <div>
                        <label className="fs-5 fw-bold me-3 " htmlFor="lote">Lote Nº</label>
                        <input id="lote" className="fs-5 text-center px-1" type="number" style={{ width: "80px" }} aria-label="Disabled input example" readonly />
                    </div>
                    <div className="ms-5">
                        <label className="fs-5 fw-bold me-3 " htmlFor="numero">Nº de Muestras</label>
                        <input id="numero" className="fs-5 text-center px-1" type="number" style={{ width: "80px" }} aria-label="Disabled input example" readonly />
                    </div>
                    <div className="ms-5 form-check d-flex align-items-center">
                        <input className="form-check-input border border-dark" type="radio" name="estado" id="estado1" checked />
                        <label className="form-check-label fs-5 fw-bold ms-3" for="estado1">
                            Interno
                        </label>
                    </div>
                    <div className="ms-5 form-check d-flex align-items-center">
                        <input className="form-check-input border border-dark" type="radio" name="estado" id="estado2" />
                        <label className="form-check-label fs-5 fw-bold ms-3" for="estado2">
                            Externo
                        </label>
                    </div>
                </div>

                <button type="button" className=" mt-2 mx-auto p-1 fw-bold bMuestra"><img className="me-2" width="60px" height="60px" src={guardar} alt="guardar" />Registrar</button>
            </form>
            <section className="align-self-start mt-auto justify-self-end">
                <Reloj grande={false} />
            </section>
        </main>
    )
}

export default Registro_Muestra