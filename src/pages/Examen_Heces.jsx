import Menu from '../components/Menu'
import User from '../components/User'
import Reloj from "../components/Reloj";
import guardar from "../assets/iconos/registro.png"


function Examen_Heces() {
  return (
    <main className='fondo_20 min-vh-100 d-flex flex-column align-items-center'>
      <section className="d-flex justify-content-between w-100">
        <Menu />
        <User grande={false} />
      </section>
      <section className=" py-3 px-3 w-95 rounded mt-1 bg-white">
        <div className='d-flex w-100 justify-content-around'>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Nombre:</b> Pedro Egnis Medina Camacho</p>
            <p className='m-0 letra_2'><b className='me-1'>C.I:</b>  30565353</p>
            <p className='m-0 letra_2'><b className='me-1'>Edad:</b>  19</p>
            <p className='m-0 letra_2'><b className='me-1'>Sexo:</b>  Masculino</p>
          </div>
          <div>
            <p className='m-0 letra_2'><b className='me-1'>Orden:</b>  6041-051</p>
            <p className='m-0 letra_2'><b className='me-1'>Paciente:</b>  0015</p>
            <p className='m-0 letra_2'><b className='me-1'>Fecha de Registro:</b>  19/04/2023</p>
            <p className='m-0 letra_2'><b className='me-1'>Medico:</b>  Juan Ramirez Gregorio Feliz |  Bioanalista</p>
          </div>
        </div>
        <div className='hr'></div>
        <div>
          <h2 className='text-center m-0'>Examen de Heces</h2>
          <h4 className='fw-bold'>Examen Macroscópico</h4>
          <div className='d-flex justify-content-between'>
            <div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">ASPECTO</label>
                <input className='ms-5 form-control border border-secondary' style={{ width: "200px", height: "30px" }} type="text" />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">COLOR</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "65px", height: "30px" }} />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">CONSISTENCIA</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "3px", height: "30px" }} />
              </div>
            </div>
            <div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-4' htmlFor="">REACCION</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "10px", height: "30px" }} />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">SANGRE</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "2px", height: "30px" }} />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">MOCO</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "15px", height: "30px" }} />
              </div>
            </div>
            <div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">PUS</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "122px", height: "30px" }} />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold' htmlFor="">RESTOS DE ALIMENTOS</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "22px", height: "30px" }} />
              </div>
              <div className='d-flex align-items-center my-1'>
                <label className='fw-bold me-5' htmlFor="">OLOR</label>
                <input className='form-control border border-secondary' type="text" style={{ width: "200px", marginLeft: "110px", height: "30px" }} />
              </div>
            </div>

          </div>
          <h4 className='m-0 fw-bold'>Examen Microscópico</h4>
          <div className='d-flex align-items-center justify-content-between my-1'>
            <div className='d-flex align-items-center my-1'>
              <label className='fw-bold me-3' htmlFor="">PROTOZOARIOS</label>
              <input className='form-control border border-secondary' type="text" style={{ width: "200px", height: "30px" }} />
            </div>
            <div className='d-flex align-items-center my-1'>
              <label className='fw-bold me-3' htmlFor="">HELMINTOS</label>
              <input className='form-control border border-secondary' type="text" style={{ width: "200px", height: "30px" }} />
            </div>
            <div className='d-flex align-items-center my-1'>
              <label className='fw-bold me-3' htmlFor="">LEUCOCITOS</label>
              <input className='form-control border border-secondary' type="text" style={{ width: "200px", height: "30px" }} />
            </div>
          </div>
          <div>
            <div className='d-flex align-items-center my-2 w-100'>
              <label className='fw-bold fs-4 me-3' htmlFor="">Examen Parasitológico</label>
              <textarea className='form-control border border-secondary' name="" rows="1"></textarea>
            </div>
            <div className='d-flex align-items-center my-2 w-100'>
              <label className='fw-bold fs-4 me-3' htmlFor="">Observaciones</label>
              <textarea className='form-control border border-secondary' name="" rows="3"></textarea>
            </div>
          </div>
        </div>
        <button className="mx-auto py-1 px-4 fw-bold bGuardar">
          <img
            className="me-2"
            width="40px"
            height="40px"
            src={guardar}
            alt="guardar"
          />
          Guardar
        </button>
      </section>
      <section className="align-self-start mt-auto justify-self-end">
        <Reloj grande={false} />
      </section>

    </main>
  )
}

export default Examen_Heces