import React from 'react'
import ReactDOM from 'react-dom/client'
import './css/index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import * as bootstrap from 'bootstrap'
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

// componenetes
import Login from './pages/Login'
import Inicio from './pages/Inicio'
import Registro_Examenes from './pages/Registro_Examenes'
import Registro_Muestra from './pages/Registro_Muestra'
import Examen_Heces from './pages/Examen_Heces'
import Examen_Orina from './pages/Examen_Orina'
import Examen_Hematologia_Completa from './pages/Examen_Hematologia_Completa'
import Resonancia_RayosX from './pages/Resonancia_RayosX'
import Consulta from './pages/Consulta'
import Captura_Resultados from './pages/Captura_Resultados'
import Examen from './pages/Examen'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path="/" element={<Inicio/>}/>
        <Route path="/Login" element={<Login/>}/>
        <Route path="/Registro_Examenes" element={<Registro_Examenes/>}/>
        <Route path="/Registro_Muestra" element={<Registro_Muestra/>}/>
        <Route path="/Consulta" element={<Consulta/>}/>
        <Route path="/Captura_Resultados" element={<Captura_Resultados/>}/>
        <Route path="/Examen_Heces" element={<Examen_Heces/>}/>
        <Route path="/Examen_Orina" element={<Examen_Orina/>}/>
        <Route path="/Examen_Hematologia_Completa" element={<Examen_Hematologia_Completa/>}/>
        <Route path="/Resonancia_RayosX" element={<Resonancia_RayosX/>}/>
        <Route path="/Examen" element={<Examen/>}/>
      </Routes>
    </Router>
  </React.StrictMode>,
)